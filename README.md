**Description:**

The proposed project is based on the analysis of massive information from social networks -twitter and instagram- in order to perform an analysis and understanding of natural language and thus be able to filter factors of interest. By "natural language" we refer to a language that is used for daily communication by human beings.
The project was set up in an Ubuntu environment using as a python language since it contains specific stable libraries-nltk-developed for natural language processing. The application was developed with the Django web framework and the results of the analysis are shown in the UI (User Interface) as geolocated markers in a Google map with the possibility of viewing the content of the text and the matching results made by the application. which is considered to be a case of interest. The cases of interest that were defined for this project were the similarity and meaning of the text with the words: "exploit", "flood", "refine" and "dilute".
The set of information to be processed was bounded to an amount stored in a PostgreSQL database provided, which is around 14,000 texts. The proposed process for content processing consists of a tour of all the texts contained in the database, for each of them:
A "tagging" is applied, this refers to processing a sequence of words, and a label is inserted for each of them giving them a lexical category (Verb, Noun, Adjective, etc).
Tagging results filter cases where words have been labeled as a noun or verb, meaning that in the natural language for the cases of interest raised are derived from them, and those who meet this condition try to infer the infinitive of the word, using the infinitive mode rule in Spanish in its 3 conjugations ending in "-ar", "-er" and "-ir".
Using the Wordnet library-a large lexical database-search and save synonyms for the infinitives of the possible matching cases found in the text
Finally, comparing, using functions provided by the same word library, the similarity between each set of word of interest case and its synonyms, against each word of possible case of matching and its synonyms, only the text will be labeled as a success case in matcheo if a similarity equal to or greater than 80% is found, when this happens they are stored in the database to avoid further re-processing
Once these matches are obtained, the last step is to try to obtain geolocation data inferring the same text and the subsequent creation of the map with the markers. In this case we try to identify entities, by means of a process called "named-entity recognition", using functions of the library nltk that identify and tagge the words, for our application we mark as categories of interest the locations (LOCATION) and organizations (ORGANIZATION ), to then check the existence and obtain coordinates using the Geocoder library with the option to make queries through the Google API






**Setup of the environment (on a linux-Ubuntu 16.04 64-bit environment):**

1- Install python
Run in console:
sudo add-apt-repository ppa:fkrull/deadsnakes-python2.7
sudo apt-get update
sudo apt-get install python2.7

2- Install nltk and django
wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python
sudo easy_install pip
sudo pip install -U nltk
sudo pip install Django


3- Download packages from nltk
Open console and type: python
Then import nltk: import nltk
Install packages: nltk.download ('all')
sudo pip install langdetect
sudo pip install pattern
sudo pip install geocoder

4- Install git and clone repository
In console:
sudo apt-get install git
Clone the repository to a local folder: git clone https: //pmaglione@bitbucket.org/pmaglione/crowd.git folder-local

5- Install postgresql and import the base
Install postgresql:
sudo apt-get install postgresql postgresql-contrib
sudo apt-get install python-psycopg2

Via console access the cloned folder of the project and import the database:
sudo su - postgres
createdb -T template0 matias_to_be_deleted
psql matias_to_be_deleted </folder/crowd_database_backup

sudo -u postgres psql matias_to_be_deleted
ALTER USER "postgres" WITH PASSWORD 'postgres';

6- Run the server
access the cloned folder of the project and run the server:
/usr/bin/python2.7 manage.py runserver 8000

7- Display the map
Open a browser and access the url:
http://localhost:8000/data/map






**Bibliography:**

Python: https://www.python.org/
Django Framework: https://www.djangoproject.com/
Library nltk: http://www.nltk.org/
Wordnet library: https://wordnet.princeton.edu/
Geocoder library: https://pypi.python.org/pypi/geocoder