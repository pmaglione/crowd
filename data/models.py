from __future__ import unicode_literals

from django.db import models

import nltk
from nltk.tree import Tree
from nltk.stem.porter import *
from nltk.corpus import wordnet as wn
from langdetect import detect
from nltk.stem.snowball import SnowballStemmer
from pattern.es import conjugate
from pattern.es import INFINITIVE
import re
import geocoder
from nltk.tokenize import TweetTokenizer
from nltk.tag.stanford import StanfordNERTagger

class Status(models.Model):
    id = models.BigIntegerField
    importinfoid = models.IntegerField
    userid = models.BigIntegerField
    geolocationid = models.IntegerField
    scopeid = models.IntegerField
    placeid = models.CharField(max_length=255)
    createdat = models.DateTimeField
    retweetid = models.BigIntegerField
    favoritecount = models.IntegerField
    inreplytoscreenname = models.CharField(max_length=255)
    inreplytostatusid = models.BigIntegerField
    inreplytouserid = models.BigIntegerField
    lang = models.CharField(max_length=255)
    retweetcount = models.IntegerField
    source = models.CharField(max_length=255)
    text = models.CharField(max_length=255)
    tweetid = models.BigIntegerField

    def __str__(self):  # __unicode__ on Python 2
        return self.text

    class Meta:
        db_table = "status"

class Place(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    placetype = models.CharField(max_length=255)
    streetaddress = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    geometrytype = models.CharField(max_length=255)
    fullname = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    countrycode = models.CharField(max_length=255)
    boundingboxtype = models.CharField(max_length=255)

    def __str__(self):  # __unicode__ on Python 2
        return self.text

    class Meta:
        db_table = "place"

class MatchedCase(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.ForeignKey(Status)
    place = models.ForeignKey(Place)
    place_name = models.CharField(max_length=255)
    lat = models.CharField(max_length=255)
    lng = models.CharField(max_length=255)
    matches = models.CharField(max_length=255)
    place_potential_object = str

    class Meta:
        db_table = "matched_case"

class LanguageManager(models.Model):
    #devuelve ISO-639 language codes
    lang_match_dictionary = {'en': 'eng', 'es': 'spa', 'it': 'ita', 'pt': 'por', 'fr': 'fra'}
    lang_name_dictionary = {'eng': 'english', 'spa': 'spanish', 'ita': 'italian', 'por': 'portuguese', 'fra': 'french'}

    def sanitize(self, text):
        #encodeo a unicode y sanitizo texto
        clean_text = text.encode('unicode_escape')
        clean_text = clean_text.replace("http://", "").replace("https://", "")
        clean_text = clean_text.replace("-", " ")
        emoji_pattern = re.compile("["
                               "\U0001F600-\U0001F64F"  # emoticons
                               "\U0001F300-\U0001F5FF"  # symbols & pictographs
                               "\U0001F680-\U0001F6FF"  # transport & map symbols
                               "\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               "]+", flags=re.UNICODE)
        return emoji_pattern.sub('', clean_text).replace("\\f", "")


    def infer(self, text):
        try:
            lang = detect(self.sanitize(text))
            if lang in ['en', 'es']:
                return self.lang_match_dictionary[lang]
            else:
                return self.lang_match_dictionary['es']
        except:
            return self.lang_match_dictionary['es']


    def get_language_by_code(self, code):
        return self.lang_name_dictionary[code]

class MatcherCommand(models.Model):
    syn_keys = [wn.synsets('explotar', lang='spa'), wn.synsets('inundar', lang='spa'), wn.synsets('refinar', lang='spa'), wn.synsets('diluviar', lang='spa')]
    stemmer = PorterStemmer()
    matches = []
    lang_guesser = LanguageManager()
    #tw = TweetTokenizer()
    #stanford_tagger = StanfordNERTagger('/home/pedro/Downloads/stanford-ner-2015-12-09/classifiers/english.all.3class.distsim.crf.ser.gz', '/home/pedro/Downloads/stanford-ner-2015-12-09/stanford-ner.jar')

    def get_stemmer(self, language):
        return SnowballStemmer(language)

    #devuelve el verbo infinitivo correcto en base a sinonimos
    #https://es.wikipedia.org/wiki/Modo_infinitivo
    def get_infinitive(self, word, text_lang):
        infinitive_terminations = ['ar', 'er', 'ir']
        word_stem = self.stemmer.stem(word)
        for termination in infinitive_terminations:
            synsets = wn.synsets(word_stem + termination, lang=text_lang)
            if (len(synsets) > 0):
                return word_stem + termination

    @classmethod
    def get_geolocation(self, text):
        lm = LanguageManager()

        text_sanitized = lm.sanitize(text)
        #chunked = self.stanford_tagger.tag(text.split())
        chunked = nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(text_sanitized)), binary=True)
        potential_named_entities = []
        named_entities = []

        # busco posibles named entities
        for t in chunked.subtrees():
            if t.label() == 'NE':
                potential_named_entities.append(' '.join([tuple[0] for tuple in t.leaves()]))

        potential_named_entities.append(location)

        # devuelva array de coordenadas de la primera
        # que se pueda geolocalizar
        for n in potential_named_entities:
            geo_result = geocoder.google(n)
            if geo_result.status == 'OK':
                return geo_result

        return []

    @classmethod
    def execute(cls, text):
        matcher = MatcherCommand()
        text_lang = cls.lang_guesser.infer(text)
        #creo el stemmer dinamicamente en base al lenguage del texto
        cls.stemmer = cls.get_stemmer(matcher, cls.lang_guesser.get_language_by_code(text_lang))

        #tokenizo y tageo
        tokens = nltk.word_tokenize(text)
        tagged = nltk.pos_tag(tokens, tagset='universal')

        # Remove punctuation,convert lower case and split into seperate words
        #tokens = re.findall(r"<a.*?/a>|<[^\>]*>|[\w'@#]+", text.lower(), flags=re.UNICODE | re.LOCALE)

        #busco raiz de los tags que son verbos, adjetivos o sustantivos
        #tag_stems = [matcher.stemmer.stem(tag[0]) for tag in tagged if tag[1] in ['NOUN', 'VERB', 'ADJ']]
        #fix pq wn.synsets no encuentra sinonimos de stem='inund' pero si de 'inundar'
        tag_stems = [matcher.get_infinitive(tag[0], text_lang) for tag in tagged if tag[1] in ['VERB', 'NOUN']]

        #busco sinonimos
        stem_synonyms = [wn.synsets(tag_stem, lang=text_lang) for tag_stem in tag_stems if tag_stem != None]

        matcher.matches = []
        #busco matches con score > 0.8
        for stem_synonym in (stem_synonym for stem_synonym in stem_synonyms if len(stem_synonym) > 0):
            for one_stem_synonym in stem_synonym:
                for syn_key in matcher.syn_keys:
                    for one_syn_key in syn_key:
                        similarity_rate = one_stem_synonym.wup_similarity(one_syn_key)
                        if (similarity_rate > 0.8):
                            matcher.matches.append(one_stem_synonym.name())


        return matcher.matches








