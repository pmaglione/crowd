from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'detect', views.detect, name='detect'),
    url(r'map', views.map, name='map'),
]