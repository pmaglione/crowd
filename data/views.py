from django.http import HttpResponse
from data.models import Status, Place, MatcherCommand, MatchedCase
from django.shortcuts import render

def detect(request):
    matches = []

    status_set = Status.objects.all()

    for status in status_set:
        result = MatcherCommand.execute(status.text)
        if len(result) > 0:
            matched = MatchedCase()
            matched.status = status
            matched.matches = ', '.join(result)
            #Si tiene placeid lo guardo sino lo calculo
            if status.placeid:
                matched.place = Place.objects.get(id=status.placeid)
            else:
                matched.place_potential_object = MatcherCommand.get_geolocation(status.text)
                if matched.place_potential_object:
                    matched.place_name = matched.place_potential_object.address
                    matched.lat = matched.place_potential_object.lat
                    matched.lng = matched.place_potential_object.lng
            matched.save()
            matches.append(matched)

    return HttpResponse()


def map(request):
    matches = MatchedCase.objects.all()

    return render(request, 'data/index.html', {'results': matches})
